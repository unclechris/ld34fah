﻿using UnityEngine;
using System.Collections;

public class GenerateLevel : MonoBehaviour
{

	public Transform spawnpoint;
	public GameObject[] dust;
	public GameObject[] walls;
	public GameObject[] shelves;
	public GameObject[] items;
	public GameObject[] gaps;
	bool lastAGap = false;
	public int lastshelf = 0;
	public int lastwall = 0;
	public int NumberSegmentsToGenerate = 20;
	public float ItemPercentage = 0.5f;
	
	// Use this for initialization
	void Start ()
	{
		CreateShelves ();
		CreateWalls ();
		CreateInitialDust ();
		StartCoroutine (InstantiateDust ());	
	}
	
	void CreateShelves ()
	{
		for (lastshelf=0; lastshelf<NumberSegmentsToGenerate; lastshelf++) {
			if (lastshelf == 0) {
				CreateShelf (shelves [0], lastshelf);
			} else {
				CreateShelf (lastshelf);
				if (Random.value <= ItemPercentage) {
					CreateItem (lastshelf);
				}
			}
		}
	}	
	
	void CreateWalls ()
	{
		for (lastwall=0; lastwall<NumberSegmentsToGenerate; lastwall++) {
			CreateWall (lastwall);
		}
		
	}	

	void CreateInitialDust ()
	{
		for (int i = -10; i < NumberSegmentsToGenerate*10; i++) {
			CreateDust (new Vector3 ((9.6f * i) + (Mathf.Abs (i) * Random.Range (0f, 6.0f)), spawnpoint.position.y, spawnpoint.position.z));
		}
	}
	
	IEnumerator InstantiateDust ()
	{
		yield return new WaitForSeconds (0.5f);
		if (spawnpoint != null && dust != null && dust.Length > 0) {
			CreateDust (spawnpoint.position);
		}
		StartCoroutine (InstantiateDust ());
	} 
	
	void CreateDust (Vector3 DustPosition)
	{
		Instantiate (dust [Random.Range (0, dust.Length)], DustPosition, Quaternion.identity);
	}
	
	//Shelf Code
	Vector3 shelfIndexToVector3 (int Index)
	{
		return  new Vector3 (Index * 9.6f, 0, 0);
	}
	

	
	void CreateShelf (int shelfIndex)
	{
		CreateShelf (shelfIndexToVector3 (shelfIndex));
		
	}
	void CreateShelf (GameObject shelfToSpawn, int shelfIndex)
	{
		CreateShelf (shelfToSpawn, shelfIndexToVector3 (shelfIndex));
	}
		
	void CreateShelf (GameObject shelftospawn, Vector3 shelveSpawnPosition)
	{
		Instantiate (shelftospawn, shelveSpawnPosition, Quaternion.identity);
	}
	
	void CreateShelf (Vector3 shelveSpawnPosition)
	{
		if (!lastAGap || Random.value < 0.80f) {
			CreateShelf (shelves [Random.Range (0, shelves.Length)], shelveSpawnPosition);
			lastAGap = false;
		} else {
			CreateShelf (gaps [Random.Range (0, gaps.Length)], shelveSpawnPosition);
			lastAGap = true;
		}
	}
	
	
	//Wall Code
	Vector3 WallIndexToVector3 (int Index)
	{
		return  new Vector3 (Index * 100f, 0, 90);
	}
	
	void CreateWall (int WallIndex)
	{
		CreateWall (WallIndexToVector3 (WallIndex));
		
	}
	void CreateWall (GameObject WallToSpawn, int WallIndex)
	{
		CreateWall (WallToSpawn, WallIndexToVector3 (WallIndex));
	}
	
	void CreateWall (GameObject Walltospawn, Vector3 WallSpawnPosition)
	{
	
		GameObject myWall = Instantiate (Walltospawn) as GameObject;
		myWall.transform.position = WallSpawnPosition;
	}
	
	void CreateWall (Vector3 WallSpawnPosition)
	{
		CreateWall (walls [Random.Range (0, walls.Length)], WallSpawnPosition);
	}

	//item code	
	Vector3 shelfIndexAndPosToVector3 (int Index, int pos)
	{
		Vector3 myReturn = shelfIndexToVector3 (Index);
		myReturn.x += (pos * 1.5f);
		return myReturn;
	}
	

	void CreateItem (int ItemIndex)
	{
		CreateItem (shelfIndexAndPosToVector3 (ItemIndex, 0));
		
	}
	void CreateItem (GameObject ItemToSpawn, int ItemIndex)
	{
		CreateItem (ItemToSpawn, shelfIndexAndPosToVector3 (ItemIndex, 0));
	}
	
	void CreateItem (GameObject Itemtospawn, Vector3 ItemSpawnPosition)
	{
		
		GameObject myItem = Instantiate (Itemtospawn) as GameObject;
		myItem.transform.position = ItemSpawnPosition;
	}
	
	void CreateItem (Vector3 ItemSpawnPosition)
	{
		int ItemIndex = (int)(ItemSpawnPosition.x / 9.6f) % items.Length;
		//int ItemIndex = Random.Range (0, items.Length);
		CreateItem (items [ItemIndex], ItemSpawnPosition);
	}
}
