﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour
{

	private bool rolling;
	public bool grounded;
	private float mass = 1.0f;
	public float speed = 1.0f;
	public float jumpSpeed = 2.0f;
	public Rigidbody2D Player ;
	public Rigidbody2D Container;
	private Vector3 myBaseScale;
	private float myBaseRadius;
	public Text Message;
	public Text Distance;
	public Text MaxDistance;
	
	public Transform groundCheck;
	public float groundRadius = 0.2f;
	public LayerMask whatIsGround;
	public float myMaxDistance;
	
	// Use this for initialization
	void Start ()
	{
		rolling = true;
		myBaseScale = Player.transform.localScale;
		myBaseRadius = groundRadius;
		myMaxDistance = PlayerPrefs.GetFloat ("myMaxDistance");
		MaxDistance.text = "Max Distance: " + myMaxDistance.ToString ("F2");
	}
	
	void Update ()
	{
		Player.transform.Rotate (0, 0, -speed); 
		
		if (rolling) {
			Player.AddForce (Vector2.right * speed);
		}
		
		if (Input.GetKey (KeyCode.A)) {
			Player.velocity = new Vector2 (0, Player.velocity.y);
			rolling = false;
		}
		
		if (Input.GetKeyUp (KeyCode.A)) {
			rolling = true;
		}
		
		if (grounded && Input.GetKey (KeyCode.D)) {
			Vector2 myJump = new Vector2 (0, jumpSpeed * speed);
			Player.AddForce (myJump);
		}
		Container.position = new Vector2 (Player.position.x, Player.position.y - 3.0f);
		Distance.text = "Distance: " + Player.position.x.ToString ("F2");
		if (transform.position.x > myMaxDistance) {
			myMaxDistance = transform.position.x;
			MaxDistance.text = "Max Distance: " + myMaxDistance.ToString ("F2");
		}
	}
	// Update is called once per frame
	void FixedUpdate ()
	{
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.tag == "pickup") {
			mass += 0.1f;
			speed += 0.1f;
			speed = Mathf.Min (speed, 10.0f);
			Player.transform.localScale = (myBaseScale * mass);
			groundRadius = myBaseRadius * mass;
			DestroyObject (coll.gameObject);
		}
	}
	
	void OnTriggerEnter2D (Collider2D  coll)
	{
		if (coll.gameObject.tag == "enemy") {
			Message.text = "You lost";
			float myTimeScale = Time.timeScale;
			Time.timeScale = 0;
			Player.velocity = Vector2.zero;
			speed = 0;
			jumpSpeed = 0;
			
			if (transform.position.x > myMaxDistance) {
				myMaxDistance = transform.position.x;
				PlayerPrefs.SetFloat ("myMaxDistance", myMaxDistance);
				Message.text = "New max distance " + myMaxDistance.ToString ("F2");

			}
			Time.timeScale = myTimeScale;
			StartCoroutine (ResetLevel ());

		}
	}
	
	IEnumerator ResetLevel ()
	{
		yield return new WaitForSeconds (2.0F);
		Application.LoadLevel ("Start");
	}
}
