﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{

	public Rigidbody2D myBody;
	public Transform myDuster;
	public float speed;
	public float SpeedIncrease;
	public float SpeedIncreaseTime;
	private float currentAngle ;
	private bool arcright = true;
	public Transform PlayerPosition;

	void Start ()
	{
		StartCoroutine (IncreaseSpeed ());
	}
	// Update is called once per frame
	void Update ()
	{
		myBody.AddForce (Vector2.right * speed);
		myDuster.eulerAngles = new Vector3 (0, 0, currentAngle);
		if ((currentAngle > 30 && arcright) || (currentAngle < -30 && (arcright == false))) {
			arcright = !arcright;
		}
		
		if (arcright) {
			currentAngle++;
		} else {
			currentAngle--;
		}

		if ((transform.position.x > PlayerPosition.transform.position.x && speed > 0)
			|| (transform.position.x < PlayerPosition.transform.position.x && speed < 0)) {
			speed = -speed;
		}
		
	}
	
	IEnumerator IncreaseSpeed ()
	{
		yield return new WaitForSeconds (SpeedIncreaseTime);
		speed += SpeedIncrease;
	}
		
		
	
}
